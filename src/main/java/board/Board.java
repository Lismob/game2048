package board;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Board<K, V> {
    private int width;
    private int height;private Map<K, V> board = new HashMap<>();
    protected Board(int width, int height) {
        this.width = width;
        this.height = height;
    }

    abstract public void fillBoard(List<V> list);

    abstract public List<K> availableSpace();

    abstract public void addItem(K key, V value);

    abstract public K getKey(int i, int j);

    abstract public V getValue(K key);

    abstract public List<K> getColumn(int j);

    abstract public List<K> getRow(int i);

    abstract public boolean hasValue(V value);

    abstract public List<V> getValues(List<K> keys);

    abstract public void replacement(List<K> keys, List<V> value);

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Map<K, V> getBoard() {
        return board;
    }

    public void setBoard(Map<K, V> board) {
        this.board = board;
    }

}