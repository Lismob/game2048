package game;

import board.Board;
import board.Key;
import board.SquareBoard;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class Game2048 implements Game {
    private GameHelper helper = new GameHelper();
    public static final int GAME_SIZE = 4;
    private final Board<Key, Integer> board = new SquareBoard<>(GAME_SIZE);

    private Random random = new Random();

    @Override
    public void init() {
        board.fillBoard(Arrays.asList());
        this.addItem();
        this.addItem();
        /** Метод иницирует начало игры. */
    }

    @Override
    public boolean canMove() {
        Game2048 game2048 = this;
        return !this.board.hasValue(null) ||
                game2048.shift(x -> game2048.getGameBoard().getRow(x)) ||
                game2048.shift(x -> game2048.getGameBoard().getColumn(x));
        /** Метод проверяет, можем ли мы делать игровой ход. */
    }

    @Override
    public boolean move(Direction direction) {
        switch (direction) {
            case LEFT:
                this.shift(x -> this.board.getRow(x));
                this.addItem();
                break;
            case RIGHT:
                this.shift(x -> {
                    List<Key> list = this.board.getRow(x);
                    Collections.reverse(list);
                    return list;
                });
                this.addItem();
                break;
            case UP:
                this.shift(x -> this.board.getColumn(x));
                this.addItem();
                break;
            case DOWN:
                this.shift(x -> {
                    List<Key> list = this.board.getColumn(x);
                    Collections.reverse(list);
                    return list;
                });
                this.addItem();
                break;
        }

        return false;
        /** Метод делает игровой ход в направлении {@param direction}. */
    }

    @Override
    public void addItem() {
        List<Key> freecells = board.availableSpace();
        int i = 2;
        if (random.nextInt(3) == 2) {
            i = 4;
        }
        int randomCell = random.nextInt(freecells.size());
        board.addItem(freecells.get(randomCell), i);
        /** Добавляет новый элемент в игру. */
    }

    @Override
    public Board getGameBoard() {
        return board;
        /** Получение игрового поля. */
    }

    @Override
    public boolean hasWin() {
        return board.hasValue(2048);
        /** Случилось ли событие победы в игре.*/
    }

    private boolean shift(Function<Integer, List<Key>> function) {
        boolean flag = false;
        for (int l = 0; l < this.board.getHeight(); l++) {
            List<Key> keysentrysheet = function.apply(l);
            List<Integer> values = board.getValues(keysentrysheet);
            List<Integer> change = helper.moveAndMergeEqual(values);
            if (!flag && !values.equals(change)) {
                flag = true;
            }
            board.replacement(keysentrysheet, change);
        }
        return flag;
    }


}

