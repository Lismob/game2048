package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameHelper {
    public List<Integer> moveAndMergeEqual(List<Integer> list) {
        List<Integer> move = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                move.add(list.get(i));
            }
        }
        for (int i = 0; i < move.size() - 1; i++) {
            if (move.get(i).equals(move.get(i + 1))) {
                move.set(i, move.get(i) + move.get(i + 1));
                move.remove(i + 1);
            }
        }
        move.addAll(Collections.nCopies(list.size() - move.size(), null));
        return move;
        /**  Метод moveAndMergeEqual должен:
         Перемещать в начало все элементы, при этом, если 2 соседних элемента равны,
         то эти элементы должны объединиться в 1. Если входной список пустой, то на выходе
         должен быть тот же пустой список.Кол-во элементов в исходном и в результирующем
         списке должно быть одинаковое. Примеры:
         1, 2, null, 3 -> 1, 2, 3, null
         2, 2, null, 3 -> 4, 3, null, null
         2, 2, 4, 4 -> 4, 8, null, null
         2, 2, 2, 3 -> 4, 2, 3, null
         2, null, null, 2 -> 4, null, null, null
         null, null, null, null -> null, null, null, null */

    }

}


