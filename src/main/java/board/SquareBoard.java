package board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SquareBoard<V> extends Board<Key, V> {
    private int size;

    public SquareBoard(int size) {
        super(size, size);
        this.size = size;
    }

    @Override
    public void fillBoard(List<V> list) {
        ArrayList<V> arrayList = new ArrayList<>(list);
        arrayList.addAll(Collections.nCopies(
                this.getHeight() * this.getWidth() - list.size(), null));
        for (int i = 0; i < this.getHeight(); i++) {
            for (int s = 0; s < this.getWidth(); s++) {
                Key key = new Key(i, s);
                this.getBoard().put(key, arrayList.get(i * getWidth() + s));
            }
        }
        /** Заполняем поле элементами из входного списка.
         * Если нужно задать пустой элемент, указываем null.
         */
    }

    @Override
    public List<Key> availableSpace() {
        List<Key> listKey = new ArrayList<>(this.size);
        for (Map.Entry<Key, V> entry : this.getBoard().entrySet()) {
            if (entry.getValue() == null) {
                listKey.add(entry.getKey());
            }
        }
        return listKey;
        /** Возвращаем все ключи, у которых значение null. */
    }

    @Override
    public void addItem(Key key, V value) {
        this.getBoard().put(key, value);
        /** Добавляем элемент {Integer value} по ключу {Key key}. */
    }

    @Override
    public Key getKey(int i, int j) {
        Key key = new Key(i, j);
//        for (Map.Entry<Key, Integer> map1 : this.getBoard().entrySet()) {
//            if(key.equals(map1.getKey())){
//            return key;}
//        }
//
//        return null;
        return this.getBoard().containsKey(key) ? key : null;
        /** Ищем уже существующий ключ по координатам {int i} {int j}. */
    }

    @Override
    public V getValue(Key key) {
        return this.getBoard().get(key);
        /** Получаем значение по {Key key} */
    }

    @Override
    public List<Key> getColumn(int j) {
        List<Key> keyColumn = new ArrayList<>();
        for (int i = 0; i < getHeight(); i++) {
            for (int k = 0; k < getWidth(); k++) {
                if (k == j) {
                    keyColumn.add(this.getKey(i, k));
                }
            }
        }
        return keyColumn;
        /** Получаем столбец ключей, по которым потом можно будет выбрать значения. */
    }

    @Override
    public List<Key> getRow(int j) {
        List<Key> keyColumn = new ArrayList<>(size);
        for (int i = 0; i < getHeight(); i++) {
            for (int k = 0; k < getWidth(); k++) {
                if (i == j) {
                    keyColumn.add(this.getKey(i, k));
                }
            }
        }
        return keyColumn;
        /** Получаем строку ключей, по которым потом можно будет выбрать значения. */
    }

    @Override
    public boolean hasValue(V value) {
        return this.getBoard().containsValue(value);
        /** Проверяем, есть ли такое значение на поле. */
    }

    @Override
    public List<V> getValues(List<Key> keys) {
        List<V> listValues = new ArrayList<>();
        for (Key key : keys) {
            if (this.getBoard().containsKey(key)) {
                listValues.add(this.getValue(key));
            }
        }
        return listValues;
        /** Получаем строку значений по строке ключей. */
    }

    @Override
    public void replacement(List<Key> keys, List<V> value) {
        for (int i = 0; i < keys.size(); i++) {
            this.getBoard().put(keys.get(i), value.get(i));
        }
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
