package game;

public enum Direction {
    RIGHT("Право"),
    LEFT("Лево"),
    DOWN("Назад"),
    UP("Вверх");
    private String Side;

    Direction(String side) {
        Side = side;
    }

    public String getSide() {
        return Side;
    }
}
